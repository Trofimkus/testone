﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using HelloWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld.Tests
{
    [TestClass()]
    public class WorkerTests
    {
        [TestMethod()]
        public void ToStringTest()
        {
            int[] mas = new int[] { 3, 5, 8, 9, 10 };
            Worker arr1 = Worker.FromArray(mas);
            Assert.AreEqual("[3, 5, 8, 9, 10]", arr1.ToString());
        }

        [TestMethod()]
        public void WorkerTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void PrintTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void SumMasTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void FromArrayTest()
        {
            int[] mas = new int[] { 3, 5, 8, 1, 13 };
            Worker arr1 = Worker.FromArray(mas);
            Assert.IsNotNull(arr1);
        }




        [TestMethod()]
        public void OddArrTest()
        {
            int[] mas = new int[] { 3, 5, 8, 9, 10 };
            Worker arr1 = Worker.FromArray(Worker.OddArr(mas));
            Assert.AreEqual("[3, 5, 9]", arr1.ToString());
        }

        [TestMethod()]
        public void EvenArrTest()
        {
            int[] mas = new int[] { 3, 5, 8, 9, 10 };
            Worker arr1 = Worker.FromArray(Worker.EvenArr(mas));
            Assert.AreEqual("[8, 10]", arr1.ToString());
        }

        [TestMethod()]
        public void GapArrTest()
        {
            int[] mas = new int[] { 3, 5, 8, 9, 10, 11, 3, 15, 4};
            int a = 4;
            int b = 11;
            Worker arr1 = Worker.FromArray(Worker.GapArr(mas,a,b));
            Assert.AreEqual("[5, 8, 9, 10]", arr1.ToString());
        }
    }
}