﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld
{
    public class Worker
    {
        private int[] arr;

        public Worker(int[] Arr)
        {
            arr = Arr;
        }

        public static Worker FromArray(int[] arr)
        {
            Worker ObjectW = new Worker(arr);
            return ObjectW;
        }

        public int[] Print(int[] arr)
        {
            return arr;
        }

        public static int SumArr(int[] arr)
        {
            int Sum = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                Sum += arr[i];
            }
            return Sum;
        }

        public override string ToString()
        {
            string NewString = "[";
            for (int i = 0; i < arr.Length; i++)
            {
                if (i != arr.Length - 1)
                {
                    NewString += arr[i] + ", ";
                }
                else
                {
                    NewString += arr[i];
                }
            }
            NewString += "]";
            return NewString;
        }

        private static int Even(int[] arr)
        {
            int n = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i]%2 == 0)
                {
                    n++;
                }
            }
            return n;
        }

        private static int Odd(int[] arr)
        {
            int n = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i]%2 != 0)
                {
                    n++;
                }
            }
            return n;
        }

        public static int[] EvenArr(int[] arr)
        {
            int[] arr2 = new int[Even(arr)];
            int j = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i]%2 == 0)
                {
                    arr2[j] = arr[i];
                    j++;
                }
            }
            return arr2;
        }

        public static int[] OddArr(int[] arr)
        {
            int[] arr2 = new int[Odd(arr)];
            int n = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] % 2 != 0)
                {
                    arr2[n] = arr[i];
                    n++;
                }
            }
            return arr2;
        }

        private static int Gap(int[] arr, int a, int b)
        {
            int n = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] > a && arr[i] < b)
                {
                    n++;
                }
            }
            return n;
        }

        public static int[] GapArr(int[] arr, int a, int b)
        {
            int[] arr2 = new int[Gap(arr,a,b)];
            int n = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] > a && arr[i] < b)
                {
                    arr2[n] = arr[i];
                    n++;
                }
            }
            return arr2;
        }
        
     }
}
        


