﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld
{
    class Program
    {
         static int[] ArrayN(int N)
         {
        int[] mas = new int[N];
            for (int i = 0; i<N; i++)
            {
                mas[i] = i + 1;
            }
        return mas;
     }


    static void Main(string[] args)
        {
            Console.Write("Enter the dimension of the array: ");
            int N = int.Parse(Console.ReadLine());
            int[] mas = ArrayN(N);
            Console.Write("An array from 1 to {0}: " + Worker.FromArray(mas).ToString() + "\n", N);
            Console.Write("Array of ODD numbers: " + Worker.FromArray(Worker.OddArr(mas))+ "\n");
            Console.Write("Array of EVEN numbers: " + Worker.FromArray(Worker.EvenArr(mas))+ "\n");
            Console.Write("The sum of te array elements: " + Worker.SumArr(Worker.EvenArr(mas)) + "\n");
            Console.Write("Enter the MIN value of the numbers: ");
            int A = int.Parse(Console.ReadLine());
            Console.Write("Enter the MAX value of the numbers: ");
            int B = int.Parse(Console.ReadLine());
            Console.Write("Array with numbers greater then {0} and less then {1}: " + Worker.FromArray(Worker.GapArr(mas,A,B)) + "\n",A,B);
            Console.WriteLine("\a");
            Console.ReadKey();
        }

     }
}
