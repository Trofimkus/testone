﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextReader
{
    class MyTextReader
    {

        public string NameFile;
        public MyTextReader(string NameFile)
        {
            this.NameFile = NameFile;

        }

        public MyText read()
        {
            MyText mytext = new MyText();
            mytext.text = File.ReadAllLines(NameFile);
            return mytext;
        }


        //public static MyTextReader read(string NameFile)
        //{
            
        //}


    }
}
