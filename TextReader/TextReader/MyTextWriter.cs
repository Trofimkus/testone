﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextReader
{
    class MyTextWriter
    {
        public string NameFile;

        public MyTextWriter(string NameFile)
        {
            this.NameFile = NameFile;
        }

        public void write(MyText text)
        {
            File.WriteAllLines(NameFile, text.text);
            
        }
    }
}
